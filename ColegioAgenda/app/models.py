from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User, Group
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

roles = {
    'PROFESOR': 'Profesor',
    'ESTUDIANTE': 'Estudiante'
}


@receiver(post_save, sender=User)
def add_superuser_profesor_group(sender, instance, created, **kwargs):
    if created:
        profesor_group = Group.objects.get(name=roles['PROFESOR'])
        estudiante_group = Group.objects.get(name=roles['ESTUDIANTE'])
        if instance.is_superuser:
            instance.groups.add(profesor_group)
        else:
            instance.groups.add(estudiante_group)
        instance.is_staff = True
        instance.save()


class ColegioAgendaUser(User):
    documento_de_identidad = models.CharField(max_length=20, unique=True, null=False)


class Profesor(ColegioAgendaUser):
    class Meta:
        verbose_name = 'Profesor'
        verbose_name_plural = 'Profesores'


@receiver(post_save, sender=Profesor)
def make_password_after_save_profesor(sender, instance, created, **kwargs):
    pwd = make_password(instance.password)
    Profesor.objects.filter(id=instance.id).update(password=pwd)


class Estudiante(ColegioAgendaUser):
    class Meta:
        verbose_name = 'Estudiante'
        verbose_name_plural = 'Estudiantes'

    def __str__(self):
        if not self.first_name:
            return self.username
        return '{} {}'.format(self.first_name, self.last_name)


@receiver(post_save, sender=Estudiante)
def make_password_after_save_estudiante(sender, instance, created, **kwargs):
    pwd = make_password(instance.password)
    Estudiante.objects.filter(id=instance.id).update(password=pwd)


YEAR_CHOICES = []
for r in range(1980, (timezone.now().year + 1)):
    YEAR_CHOICES.append((r, r))


class Curso(models.Model):
    ANIO_CHOICES = YEAR_CHOICES
    nombre = models.CharField(max_length=140)
    anio = models.IntegerField('año', choices=ANIO_CHOICES, default=timezone.now().year)

    def __str__(self):
        return '{} {}'.format(self.nombre, self.anio)

    class Meta:
        verbose_name = 'Curso'
        verbose_name_plural = 'Cursos'


class CursoMatricula(models.Model):
    estudiante = models.ForeignKey(Estudiante)
    curso = models.ForeignKey(Curso)

    def __str__(self):
        return self.curso.nombre

    class Meta:
        verbose_name = 'Matricula'
        verbose_name_plural = 'Matriculas'


class Actividad(models.Model):
    curso = models.ForeignKey(Curso)
    descripcion = models.CharField(max_length=140)
    archivo = models.FileField('archivo', upload_to='./')

    def __str__(self):
        return '{} - {}'.format(
            self.curso,
            self.descripcion
        )

    class Meta:
        verbose_name = 'Actividad'
        verbose_name_plural = 'Actividades'


class SolucionActividad(models.Model):
    actividad = models.ForeignKey(Actividad)
    archivo = models.FileField('archivo', upload_to='./')
    observacion = models.TextField()
    nota = models.FloatField(default=0)
    estudiante = models.ForeignKey(Estudiante, null=True, blank=True)

    def __str__(self):
        estudiante = self.estudiante
        return '{} - {}'.format(
            estudiante.username if estudiante.username else '{} {}'.format(
                estudiante.first_name, estudiante.last_name
            ),
            self.actividad.curso.nombre
        )

    class Meta:
        verbose_name = 'Solución actividad'
        verbose_name_plural = 'Soluciones actividades'


class Calificacion(models.Model):
    solucion = models.OneToOneField(SolucionActividad)
    nota = models.FloatField(default=0)

    class Meta:
        verbose_name = 'Calificación'
        verbose_name_plural = 'Calificaciones'


@receiver(post_save, sender=Calificacion)
def update_calificacion_solucion_estudiante(sender, instance, created, **kwargs):
    if created:
        obj = SolucionActividad.objects.get(pk=instance.solucion.id)
        obj.nota = instance.nota
        obj.save()
