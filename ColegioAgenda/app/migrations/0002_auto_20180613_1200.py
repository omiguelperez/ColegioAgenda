# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='actividad',
            name='curso',
            field=models.ForeignKey(default=1, to='app.Curso'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='calificacion',
            name='actividad',
            field=models.OneToOneField(default=1, to='app.Actividad'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='curso',
            name='nombre',
            field=models.CharField(max_length=140, default='-'),
            preserve_default=False,
        ),
    ]
