# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_auto_20180613_1506'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='actividad',
            name='curso_matricula',
        ),
        migrations.AddField(
            model_name='actividad',
            name='curso',
            field=models.ForeignKey(to='app.Curso', default=1),
            preserve_default=False,
        ),
    ]
