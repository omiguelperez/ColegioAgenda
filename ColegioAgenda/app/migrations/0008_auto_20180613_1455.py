# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_auto_20180613_1446'),
    ]

    operations = [
        migrations.AlterField(
            model_name='solucionactividad',
            name='estudiante',
            field=models.ForeignKey(blank=True, null=True, to='app.Estudiante'),
        ),
    ]
