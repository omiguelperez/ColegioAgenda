# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_auto_20180613_1223'),
    ]

    operations = [
        migrations.CreateModel(
            name='SolucionActividad',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('archivo', models.FileField(upload_to='./', verbose_name='archivo')),
                ('observacion', models.TextField()),
                ('nota', models.FloatField(default=0)),
                ('actividad', models.OneToOneField(to='app.Actividad')),
            ],
        ),
    ]
