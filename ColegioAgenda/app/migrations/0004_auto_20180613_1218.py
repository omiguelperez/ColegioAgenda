# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20180613_1206'),
    ]

    operations = [
        migrations.CreateModel(
            name='CursoMatricula',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('curso', models.ForeignKey(to='app.Curso')),
                ('estudiante', models.ForeignKey(to='app.Estudiante')),
            ],
            options={
                'verbose_name': 'Matricula',
                'verbose_name_plural': 'Matriculas',
            },
        ),
        migrations.RemoveField(
            model_name='actividad',
            name='curso',
        ),
        migrations.AddField(
            model_name='actividad',
            name='curso_matricula',
            field=models.ForeignKey(default=1, to='app.CursoMatricula'),
            preserve_default=False,
        ),
    ]
