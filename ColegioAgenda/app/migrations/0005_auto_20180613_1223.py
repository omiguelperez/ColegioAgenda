# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0004_auto_20180613_1218'),
    ]

    operations = [
        migrations.AddField(
            model_name='calificacion',
            name='curso_matricula',
            field=models.OneToOneField(to='app.CursoMatricula', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='calificacion',
            name='nota',
            field=models.FloatField(default=0),
        ),
    ]
