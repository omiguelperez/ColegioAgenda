# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_solucionactividad'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='solucionactividad',
            options={'verbose_name': 'Solución actividad', 'verbose_name_plural': 'Soluciones actividades'},
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='actividad',
        ),
        migrations.RemoveField(
            model_name='calificacion',
            name='curso_matricula',
        ),
        migrations.AddField(
            model_name='calificacion',
            name='solucion',
            field=models.OneToOneField(default=1, to='app.SolucionActividad'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='solucionactividad',
            name='estudiante',
            field=models.ForeignKey(null=True, to='app.Estudiante'),
        ),
        migrations.AlterField(
            model_name='solucionactividad',
            name='actividad',
            field=models.ForeignKey(to='app.Actividad'),
        ),
    ]
