# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20180613_1200'),
    ]

    operations = [
        migrations.AddField(
            model_name='actividad',
            name='archivo',
            field=models.FileField(default=1, verbose_name='archivo', upload_to='./'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='actividad',
            name='descripcion',
            field=models.CharField(default=1, max_length=140),
            preserve_default=False,
        ),
    ]
