from django.contrib import admin
from django.contrib.auth.models import User, Group

from app.models import (
    Profesor,
    Estudiante,
    Actividad,
    Calificacion,
    Curso,
    CursoMatricula,
    SolucionActividad
)


class ActividadAdmin(admin.ModelAdmin):
    list_display = ['archivo', 'curso', 'descripcion']


class CursoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'anio']


class CalificacionAdmin(admin.ModelAdmin):
    list_display = ['solucion', 'nota']


class EstudianteAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'documento_de_identidad', 'email', 'first_name', 'last_name']


class ProfesorAdmin(admin.ModelAdmin):
    list_display = ['id', 'username', 'documento_de_identidad', 'email', 'first_name', 'last_name']


class CursoMatriculaAdmin(admin.ModelAdmin):
    list_display = ['id', 'estudiante', 'curso']


class SolucionActividadAdmin(admin.ModelAdmin):
    list_display = ['archivo', 'actividad', 'nota']

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'estudiante', None) is None:
            estudiante_id = request.user.id
            estudiante = Estudiante.objects.get(pk=estudiante_id)
            obj.estudiante = estudiante
        obj.save()


admin.site.site_header = 'Colegio-Agenda'

admin.site.unregister(User)
admin.site.unregister(Group)

admin.site.register(Profesor, ProfesorAdmin)
admin.site.register(Estudiante, EstudianteAdmin)
admin.site.register(Actividad, ActividadAdmin)
admin.site.register(Calificacion, CalificacionAdmin)
admin.site.register(Curso, CursoAdmin)
admin.site.register(CursoMatricula, CursoMatriculaAdmin)
admin.site.register(SolucionActividad, SolucionActividadAdmin)
